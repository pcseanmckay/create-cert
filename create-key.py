from certsrv import Certsrv

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography import x509
from cryptography.x509.oid import NameOID
from cryptography.hazmat.primitives import hashes

import argparse

# This function parses the command line arguments
def parseCommandLine():
    parser = argparse.ArgumentParser(
        description="Script to create a private key in PEM format"
    )
    parser.add_argument('-o', dest='outfile', help='Full pathname to file to save private key data to')

    args = parser.parse_args()

    return args

######################################### Start of main processing

if __name__ == "__main__":
    
    #logging.basicConfig(level=logging.INFO, format='[%(levelname)s]: %(message)s')
    args = parseCommandLine()

    outfile = args.outfile

    # Generate a key
    key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=2048,
        backend=default_backend()
    )

    # Save the key to a file
    pem_key = key.private_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PrivateFormat.TraditionalOpenSSL,
                encryption_algorithm=serialization.NoEncryption(),
    )

    with open(outfile, 'wb') as open_file:
        open_file.write(pem_key)