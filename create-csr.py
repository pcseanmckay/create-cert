from certsrv import Certsrv

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
#from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography import x509
from cryptography.x509.oid import NameOID
from cryptography.hazmat.primitives import hashes
from cryptography.exceptions import UnsupportedAlgorithm as CEUnsupportedAlgorithm

import argparse

# This function parses the command line arguments
def parseCommandLine():
    parser = argparse.ArgumentParser(
        description="Script to create a CSR in PEM format"
    )
    parser.add_argument('-k', dest='key', help='Full pathname of private key file to load')
    parser.add_argument('-s', dest='sites', help='A comma separated list of site names the CSR is for')
    parser.add_argument('-o', dest='outfile', help='Full pathname to file to save CSR data to')

    args = parser.parse_args()

    return args

######################################### Start of main processing

if __name__ == "__main__":
    
    #logging.basicConfig(level=logging.INFO, format='[%(levelname)s]: %(message)s')
    args = parseCommandLine()

    key_file = args.key
    sites = args.sites
    outfile = args.outfile

    key_error = False

    try:
        # Load key from file
        with open(key_file, 'rb') as open_file:
            key = serialization.load_pem_private_key(
                open_file.read(),
                password=None,
                backend=default_backend()
            )
    except FileNotFoundError:
        print("Key file {} was not found...".format(key_file))
        key_error = True
    except ValueError:
        print("Key PEM data could not be decode...")
        key_error = True
    except TypeError:
        print("Key password error was detected...")
        key_error = True
    except CEUnsupportedAlgorithm:
        print("Key algorithm is not supported...")
        key_error = True
    
    if key_error:
        quit()

    # Parse sites into an array
    sites_array = sites.split(',')
    
    # Create an array of DNSNames for the sites in sites_array for passing to SubjectAlternativeName
    DNSNames_array = []

    for site in sites_array:
        DNSNames_array.append(x509.DNSName(site))

    # Generate a CSR
    csr = x509.CertificateSigningRequestBuilder().subject_name(x509.Name([
        x509.NameAttribute(NameOID.COMMON_NAME, sites_array[0]),
    ])).add_extension(
        x509.SubjectAlternativeName(
            DNSNames_array,
        ),
        critical=False,
    ).sign(key, hashes.SHA256(), default_backend())

    # Need to check for the existence of a file, load it and check its hash against the current CSR.
    # If the hashes don't match, write out the new CSR file.
    save_csr = False
    try:
        # Try to load the old CSR file
        with open(outfile, 'rb') as open_file:
            old_csr = x509.load_pem_x509_csr(
                open_file.read(),
                backend=default_backend()
            )
    except FileNotFoundError:
        print("CSR file not found, saving file...")
        save_csr = True
    except ValueError:
        print("CSR file PEM data could not be decoded, overwriting...")
        save_csr = True

    if save_csr == False:
        # Here we loaded an old CSR. Now we need to compare hashes of the data to see if they are the same.
        # If they are different, we should set the save flag to save the new CSR to file.
        old_csr_digest = hashes.Hash(hashes.SHA256(), backend=default_backend())
        old_csr_digest.update(old_csr.public_bytes(serialization.Encoding.PEM))
        old_csr_digest_bytes = old_csr_digest.finalize()

        new_csr_digest = hashes.Hash(hashes.SHA256(), backend=default_backend())
        new_csr_digest.update(csr.public_bytes(serialization.Encoding.PEM))
        new_csr_digest_bytes = new_csr_digest.finalize()

        if old_csr_digest_bytes != new_csr_digest_bytes:
            save_csr = True

    if save_csr == True:    
        # Need to save the CSR file here...
        with open(outfile, 'wb') as open_file:
            open_file.write(csr.public_bytes(serialization.Encoding.PEM))