from certsrv import Certsrv

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
#from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography import x509
#from cryptography.x509.oid import NameOID
from cryptography.hazmat.primitives import hashes

import argparse

# This function parses the command line arguments
def parseCommandLine():
    parser = argparse.ArgumentParser(
        description="Script to send CSR request to an AD Certificate Enrollment webserver and retrieve resulting cert"
    )
    parser.add_argument('-c', dest='csr_file', help='Full pathname to CSR file to load')
    parser.add_argument('-s', dest='server', help='Name of AD server with Certificate Enrollment that CSR is sent to')
    parser.add_argument('-u', dest='username', help='Username to use for authentication')
    parser.add_argument('-p', dest='password', help='Password to use for authentication')
    parser.add_argument('-o', dest='outfile', help='Full pathname to save certificate data to')

    args = parser.parse_args()

    return args

######################################### Start of main processing

if __name__ == "__main__":
    
    #logging.basicConfig(level=logging.INFO, format='[%(levelname)s]: %(message)s')
    args = parseCommandLine()

    csr_file = args.csr_file
    server = args.server
    username = args.username
    password = args.password
    outfile = args.outfile

    csr_error = False

    try:
        # Load csr from file
        with open(csr_file, 'rb') as open_file:
            csr = x509.load_pem_x509_csr(open_file.read(), default_backend())
    except FileNotFoundError:
        print("CSR file not found...")
        csr_error = True
    except ValueError:
        print("CSR file PEM data could not be decoded...")
        csr_error = True

    if csr_error:
        quit()

    # Get the cert from the ADCS server
    pem_req = csr.public_bytes(serialization.Encoding.PEM)

    ca_server = Certsrv(server, username, password)

    cert_error = False
    try:
        pem_cert = ca_server.get_cert(pem_req, "WebServer")
    except Certsrv.RequestDeniedException:
        print("Cert request denied...")
        cert_error = True
    except Certsrv.CertificatePendingException:
        print("Cert requires admin approval...")
        cert_error = True
    except Certsrv.CouldNotRetrieveCertificateException:
        print("Could not retrieve the cert...")
        cert_error = True
    
    if cert_error:
        quit()

    # Need to check for the existence of a file, load it and check its hash against the current cert.
    # If the hashes don't match, write out the new cert file.
    save_cert = False
    try:
        # Try to load the old CSR file
        with open(outfile, 'rb') as open_file:
            old_cert = x509.load_pem_x509_certificate(
                open_file.read(),
                backend=default_backend()
            )
    except FileNotFoundError:
        print("Cert file not found, saving file...")
        save_cert = True
    except ValueError:
        print("Cert file PEM data could not be decoded, overwriting...")
        save_cert = True

    if save_cert == False:
        # Here we loaded an old cert. Now we need to compare hashes of the data to see if they are the same.
        # If they are different, we should set the save flag to save the new cert to file.
        old_cert_digest = hashes.Hash(hashes.SHA256(), backend=default_backend())
        old_cert_digest.update(old_cert.public_bytes(serialization.Encoding.PEM))
        old_cert_digest_bytes = old_cert_digest.finalize()

        new_cert_digest = hashes.Hash(hashes.SHA256(), backend=default_backend())
        new_cert_digest.update(pem_cert.public_bytes(serialization.Encoding.PEM))
        new_cert_digest_bytes = new_cert_digest.finalize()

        if old_cert_digest_bytes != new_cert_digest_bytes:
            save_cert = True

    if save_cert == True:    
        # Need to save the CSR file here...
        with open(outfile, 'wb') as open_file:
            open_file.write(pem_cert.public_bytes(serialization.Encoding.PEM))